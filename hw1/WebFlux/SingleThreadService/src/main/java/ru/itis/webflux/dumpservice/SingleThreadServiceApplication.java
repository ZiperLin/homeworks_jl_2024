package ru.itis.webflux.dumpservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SingleThreadServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(SingleThreadServiceApplication.class, args);
    }

}
