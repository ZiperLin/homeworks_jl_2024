package ru.itis.webflux.dumpservice.models;


public class Model {

    public String text;

    private Model(String text) {
        this.text = text;
    }

    public static Model createItem(String text) {
        return new Model(text);
    }
}
