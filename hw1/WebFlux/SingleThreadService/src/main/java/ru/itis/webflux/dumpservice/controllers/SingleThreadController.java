package ru.itis.webflux.dumpservice.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.webflux.dumpservice.models.Model;

import java.util.List;

@RestController
@RequestMapping("/all")
public class SingleThreadController {

    @GetMapping()
    public List<Model> runSingle() {
        runTaskWithDelay();
        return List.of(Model.createItem("text1slow"), Model.createItem("text2slow"));
    }

    private void runTaskWithDelay() {
        long start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start < (long) 7000) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
                throw new IllegalStateException(e);
            }
        }
    }
}
