package ru.itis.webflux.smartservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SmartServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SmartServiceApplication.class, args);
	}

}
