package ru.itis.webflux.smartservice.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.webflux.smartservice.models.Model;

import java.util.List;

@RestController()
@RequestMapping("/all")
public class SeveralThreadController {

    @GetMapping()
    public List<Model> runSeveral() {
        return List.of(Model.createModel("text1fast"), Model.createModel("text2fast"));
    }
}
