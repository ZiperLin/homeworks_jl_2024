package ru.itis.webflux.smartservice.models;

public class Model {

    public String text;

    private Model(String text) {
        this.text = text;
    }

    public static Model createModel(String text) {
        return new Model(text);
    }
}
