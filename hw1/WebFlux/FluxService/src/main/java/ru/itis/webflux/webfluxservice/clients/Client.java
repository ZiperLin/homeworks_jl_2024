package ru.itis.webflux.webfluxservice.clients;

import reactor.core.publisher.Flux;
import ru.itis.webflux.webfluxservice.models.Model;


public interface Client {

    Flux<Model> getAll();

}
