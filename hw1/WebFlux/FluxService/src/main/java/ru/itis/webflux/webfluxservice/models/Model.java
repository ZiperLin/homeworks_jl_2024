package ru.itis.webflux.webfluxservice.models;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class Model {
    public String text;
}
