package ru.itis.webflux.webfluxservice.services;

import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.scheduler.Schedulers;
import ru.itis.webflux.webfluxservice.clients.Client;
import ru.itis.webflux.webfluxservice.models.Model;

import java.util.ArrayList;
import java.util.List;

@Component
public class ServiceImpl implements Service {

    private final List<Client> clients;

    public ServiceImpl(List<Client> clients) {
        this.clients = clients;
    }


    @Override
    public Flux<Model> getAll() {
        List<Flux<Model>> fluxes = new ArrayList<>();
        for (Client client : clients) {
            Flux<Model> all = getAll(client);
            fluxes.add(all);
        }
        return Flux.merge((fluxes));
    }

    private Flux<Model> getAll(Client client) {
        return client.getAll()
                .subscribeOn(Schedulers.boundedElastic());
    }

}
