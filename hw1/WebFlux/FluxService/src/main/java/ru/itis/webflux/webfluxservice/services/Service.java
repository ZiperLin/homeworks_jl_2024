package ru.itis.webflux.webfluxservice.services;


import reactor.core.publisher.Flux;
import ru.itis.webflux.webfluxservice.models.Model;


public interface Service {
    Flux<Model> getAll();
}
