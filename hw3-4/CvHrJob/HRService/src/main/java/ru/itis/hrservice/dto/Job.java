package ru.itis.hrservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Job {
    private String name;
    private Integer experience;
    private List<String> stack;
    private String description;
    private String company;
    private String proposedSalary;
}