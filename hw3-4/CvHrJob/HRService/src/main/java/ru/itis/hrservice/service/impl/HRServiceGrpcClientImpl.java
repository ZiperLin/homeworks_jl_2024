package ru.itis.hrservice.service.impl;
import lombok.RequiredArgsConstructor;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;
import ru.itis.hrservice.dto.Cv;
import ru.itis.hrservice.dto.CvJobPair;
import ru.itis.hrservice.dto.Job;
import ru.itis.hrservice.grpc.pb.cv.CVServiceGrpc;
import ru.itis.hrservice.grpc.pb.job.JobServiceGrpc;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HRServiceGrpcClientImpl implements ru.itis.hrservice.service.HRService {

    @GrpcClient("JobService")
    private JobServiceGrpc.JobServiceBlockingStub jobService;

    @GrpcClient("CvService")
    private CVServiceGrpc.CVServiceBlockingStub cvService;

    @Override
    public List<CvJobPair> getPairs() {
        List<CvJobPair> pairs = new ArrayList<>();

        GetAllResponse jobsResponse = jobService.getAll(GetAllRequest.newBuilder().build());

        List<Job> jobs = jobsResponse.getJobsList().stream().map(j -> Job.builder()
                .name(j.getName())
                .experience(j.getExperience())
                .stack(j.getStackList())
                .description(j.getDescription())
                .company(j.getCompany())
                .proposedSalary(j.getProposedSalary())
                .build()).toList();

        ru.itis.hrservice.grpc.pb.cv.GetAllResponse cvsResponse = cvService.getAll(ru.itis.hrservice.grpc.pb.cv.GetAllRequest.newBuilder().build());

        List<Cv> cvs = cvsResponse.getCvsList().stream().map(c -> Cv.builder()
                .firstName(c.getFirstName())
                .lastName(c.getLastName())
                .skills(c.getStackList())  // Обновлено для работы с новым полем
                .experience(c.getExperience())
                .profession(c.getProfession())
                .build()).toList();

        for (Job job : jobs) {
            for (Cv cv : cvs) {
                boolean enoughExperience = cv.getExperience() >= job.getExperience();
                boolean enoughSkills = true;
                if (enoughExperience) {
                    for (String skill : job.getStack()) {
                        if (!cv.getSkills().contains(skill)) {
                            enoughSkills = false;
                            break;
                        }
                    }
                    if (enoughSkills) {
                        pairs.add(CvJobPair.builder()
                                .cv(cv)
                                .job(job)
                                .build());
                    }
                }
            }
        }

        return pairs;
    }
}