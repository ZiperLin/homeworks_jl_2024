package ru.itis.hrservice.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Cv {
    private String name;
    private String profession;
    private Integer experience;
    private List<String> stack;
    private List<String> links;
    private List<String> companies;
}