package core

import "go.mongodb.org/mongo-driver/bson/primitive"

type CV struct {
	ID         primitive.ObjectID `bson:"_id,omitempty" json:"id"`
	Name       string             `bson:"name,omitempty" json:"name"`
	Profession string             `bson:"profession,omitempty" json:"profession"`
	Experience int32              `bson:"experience,omitempty" json:"experience"`
	Stack      []string           `bson:"stack,omitempty" json:"stack"`
	Links      []string           `bson:"links,omitempty" json:"links"`
	Companies  []string           `bson:"companies,omitempty" json:"companies"`
}
